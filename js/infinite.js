function loadMoreContent()
{
    $.get('pages/content.html', function(data) {
        if (data != '') {
            $('#content .col-md-4:last').after(data);
        }
    });
};

$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        loadMoreContent();
    }
});
