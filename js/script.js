var snitchApp = angular.module('snitchApp', ['ngRoute']);

snitchApp.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'pages/about.html',
            controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'pages/contact.html',
            controller  : 'contactController'
        })

        .when('/products', {
            templateUrl : 'pages/products.html',
            controller  : 'productsController'
        });

});

snitchApp.controller('mainController', function($scope) {
});

snitchApp.controller('aboutController', function($scope) {
});

snitchApp.controller('contactController', function($scope) {
});
snitchApp.controller('productsController', function($scope) {
});

